FROM centos/php-72-centos7

MAINTAINER "Björn Dieding" <bjoern@xrow.de>

USER root

ADD . /opt/app-root/src/
ENV APP_ROOT=/opt/app-root/src
ENV LANG=en_US.UTF-8
ENV KUBECONFIG="$HOME/.kube/config"

ENV PATH=/opt/rh/rh-mariadb102/root/usr/bin:${APP_ROOT}/bin:$PATH
# default variable for sync.sh - can be overwritten by kubernetes/openshift
ENV KEY=/root/.ssh/id_rsa
ENV TARGET=mysql://root:ezplatform@localhost:3306/ezplatform
RUN yum install -y centos-release-scl-rh && \
    INSTALL_PKGS="rsync tar gettext hostname bind-utils gzip rh-mariadb102  sshpass openssh openssh-clients epel-release nmap-ncat" && \
    yum install -y --setopt=tsflags=nodocs $INSTALL_PKGS && \
    yum install -y jq percona-xtrabackup.x86_64 && \
    rpm -V $INSTALL_PKGS && \
    yum clean all && \
    chmod 777 ${APP_ROOT}/ && \
    chmod 755 ${APP_ROOT}/sync.php && \
    wget https://raw.githubusercontent.com/composer/getcomposer.org/master/web/installer -O - -q | php -- --quiet --install-dir=/usr/bin --filename=composer && \
    composer install

RUN OC_DOWNLOAD_URL="https://github.com/openshift/origin/releases/download/v3.11.0/openshift-origin-client-tools-v3.11.0-0cbc58b-linux-64bit.tar.gz" \
 && TMP=$(mktemp -d) \
 && curl -sSfL $OC_DOWNLOAD_URL | tar -xz --strip-components=1 -C $TMP \
 && mv $TMP/{oc,kubectl} /usr/local/bin \
 && rm -Rf $TMP \
 && mkdir $(dirname $KUBECONFIG) \
 && update-ca-trust extract

COPY bin/ ${APP_ROOT}/bin/
RUN chmod -R u+x ${APP_ROOT}/bin && \
    chgrp -R 0 ${APP_ROOT} && \
    chmod -R g=u ${APP_ROOT} /etc/passwd
USER 1001
ENTRYPOINT [ "uid_entrypoint" ]
CMD [ "/bin/sh", "-c", "/opt/app-root/src/sync.php"]
