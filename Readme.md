# How to use


```bash
export CONNECTION="ssh://root:centos@database:22"
export SOURCE="mysql://root@localhost:3306/databasename"
sync.php
```

or 

```bash
export CONNECTION="ssh://bdieding@somehost:22"
export KEY="id_rsa"
export SOURCE="mysql://root:password@localhost:3306/databasename"
export TARGET="mysql://root@target:3306/databasename"
sync.php
```
