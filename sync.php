#!/usr/bin/env php
<?php

require __DIR__ . '/vendor/autoload.php';

use phpseclib\Net\SSH2;
use phpseclib\Crypt\RSA;

define('NET_SSH2_LOGGING', 5);

$source_arg = getenv ( "SOURCE" );
$target_arg = getenv ( "TARGET" );
$key = getenv ( "KEY" );
$sync = new Sync( getenv ( "CONNECTION" ), getenv ( "KEY" ) );
if (! $sync->ssh ){
  echo "No SSH Connection given.\n";
}

if($sync->ssh)
{
    echo "Using connection / host: " . getenv ( "CONNECTION" ) . " with host key " . $sync->ssh->getServerPublicHostKey() . "\n";
    if($sync->ssh->exec('innobackupex --help > /dev/null')){
        echo "Feature innobackupex available.\n";
    }
    if($sync->ssh->exec("mysqldump --help > /dev/null")){
        echo "Feature mysqldump available.\n";
    }
    if($sync->ssh->exec("rsync --help > /dev/null")){
        echo "Feature rsync available.\n";
    }
}
if (strpos($source_arg, "mysql://") !== false) {
    echo "SOURCE MYSQL: $source_arg.\n";
    $sync->copyDatabase(getenv ( "SOURCE" ), $target_arg );
}
elseif (strpos($source_arg, "rsync://") !== false) {
    echo "SOURCE RSYNC: $source_arg.\n";
    $sync->copyFiles(getenv ( "SOURCE" ), $target_arg );
}
else{
    echo "Protocol of SOURCE (" . $source_arg . ") not known.";
    exit(1);
}
echo "Done.\n";
exit(0);

class Sync
{
    public function __construct( string $connection = null, string $key = null )
    {
        if(getenv("KUBECONFIG_CONTENT"))
        {
            $this->configureKube();
        }
        if ( $connection ) {
            $this->connect( $connection, $key );
        }
    }
    public $filename = "dump.sql.gz";
    public $ssh = null;
    public $key = null;
    public $connection = null;
    public function copyFiles(string $source_arg, string $target_arg )
    {
        $connection = parse_url( $this->connection );
        $target = parse_url( $target_arg );
        $source = parse_url( $source_arg );
        $cmd  = "";
        $ssh = "";
        if( !empty( $connection['pass'] ) ){
            $cmd  .= "sshpass -p \"".$connection['pass']."\" ";
            $ssh = "-e 'ssh -q -o StrictHostKeyChecking=no'";
        }else{
            file_put_contents("id_rsa", $this->key);
            chmod( "id_rsa",0600);
            $ssh  = "-e \"/usr/bin/ssh -i id_rsa -q -o StrictHostKeyChecking=no -l ". $connection['user'] . ":".$connection['host']." -p ".$connection['port']."\"";
        }
        $tpath = $target['path'];
        if( !file_exists( $tpath ) ){
            mkdir( $tpath, 0777, true);
        }
        $path = rtrim( $source['path'] , "/") . "/*";
        $cmd  .= "/usr/bin/rsync -avz --no-o --no-g --chmod=Du=rwx,Dg=rwx,Do=rwx,Fu=rw,Fg=rw,Fo=rw --omit-dir-times --omit-link-times --exclude '**/cache/' --exclude '**/_aliases/' --exclude '**/logs/' -q $ssh " . $connection['user'] . "@" . $connection['host'] . ":" . $path . " " . $tpath;
        echo $cmd . "\n";
        echo "Start rsyncing files.\n";
        system($cmd);
    }
    public function copyDatabase(string $source_arg, string $target_arg )
    {
        if( $this->ssh->exec("mysqldump --help") && $this->ssh->getExitStatus() == 0 ){
            echo "Feature mysqldump available.\n";
        }else{
            echo "Error: Feature mysqldump not available.\n";
            exit(1);
        }
        if( $this->ssh->exec("gzip --help") && $this->ssh->getExitStatus() == 0 ){
            echo "Feature gzip available.\n";
        }else{
            echo "Error: Feature gzip not available.\n";
            exit(1);
        }
        if ( empty ( $target_arg ) ){
            $target_arg = "mysql://root:ezplatform@localhost:3306/ezplatform";
        }
        $target = parse_url( $target_arg );
        $source = parse_url( $source_arg );

        $this->ssh->exec( "printf '%s\\n' '[client]' 'password=". $source['pass'] . "' >  ~/.mysql.backup.cnf; chmod 600  ~/.mysql.backup.cnf" );
        $dump = "mysqldump --defaults-file=~/.mysql.backup.cnf --compress --no-create-db --default-character-set=utf8mb4 --routines --triggers --single-transaction -h " . $source['host'] ;
        if (!empty($source['user'])) {
            $dump .= " -u " . $source['user'];
        }
        if (! empty($source['port'])) {
            $dump .= " --port=" . $source['port'];
        }
        $dump .= " " . ltrim( $source['path'] , "/");

        $filename = "dump.sql.gz";
        @unlink($filename);
        if (!$handle = fopen($filename, "c")) {
            print "Kann die Datei $filename nicht öffnen";
            exit;
        }
        $packet_handler = function($str) use ($handle)
        {
            fwrite($handle, $str);
        };
        echo "-- Start mysqldump --\n";
        echo $dump . "| gzip -f -9 -c\n";
 
        $this->ssh->exec($dump . "| gzip -f -9 -c", $packet_handler );
        fclose($handle);
        echo "-- End mysqldump --\n";
        $mysqlcmd = "mysql --compress --init-command=\"SET SESSION FOREIGN_KEY_CHECKS=0;\" ";
        if (!empty($target['user'])) {
            $mysqlcmd .= " -u " . $target['user'];
        }
        if (!empty($target['pass'])) {
            $mysqlcmd .= " -p'" . $target['pass']."'";
        }
        if (! empty($target['port'])) {
            $mysqlcmd .= " --port=" . $target['port'];
        }
        if (! empty($target['host'])) {
            $mysqlcmd .= " -h" . $target['host'];
        }

        echo "-- Start mysql insert --\n";
        $targetdb = ltrim( $target['path'] , "/");
        $cmd = $mysqlcmd . " -e  \"DROP DATABASE IF EXISTS ezplatform_tmp;\"";
        echo "$cmd\n";
        system($cmd);
        $cmd = $mysqlcmd . " -e  \"CREATE DATABASE IF NOT EXISTS ezplatform_tmp CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;\"";
        echo "$cmd\n";
        system($cmd);
        $cmd = "gunzip < " . $filename . " | " . $mysqlcmd . " ezplatform_tmp";
        echo "$cmd\n";
        system($cmd);
        //create database if it does not exist yet
        system($mysqlcmd . " -e  \"CREATE DATABASE IF NOT EXISTS " . $targetdb . " CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;\"");
        //alter charset anyways because the database might be created with a wrong charset
        system($mysqlcmd . " -e  \"ALTER DATABASE " . $targetdb . " CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;\"");
        system($mysqlcmd . " ezplatform_tmp -sNe 'show tables' | while read table; do " . $mysqlcmd . " -h" . $target['host'] ." -e \"DROP TABLE IF EXISTS ".$targetdb.".\$table\"; done");
        system($mysqlcmd . " ezplatform_tmp -sNe 'show tables' | while read table; do " . $mysqlcmd . " -h" . $target['host'] ." -e \"RENAME TABLE ezplatform_tmp.\$table TO " . $targetdb . ".\$table\"; done");
        system($mysqlcmd . " -e  \"DROP DATABASE IF EXISTS ezplatform_tmp;\"");
        echo "-- End mysql insert --\n";
        
        @unlink("dump.sql.gz" );
    }
    private static function findDBHost()
    {
      foreach ( array_keys( getenv() ) as $name )
      {
        if ( strpos ( $name, "MYSQL_SERVICE_HOST" ) !== false ){
          return getenv($name);
        }
        if ( strpos ( $name, "MARIADB_SERVICE_HOST" ) !== false ){
          return getenv($name);
        }
      }
      return false;
    }
    private function connect(string $connection, string $key = null )
    {
        $this->connection = $connection;
        $connection_array = parse_url( $connection );

            $ssh = new SSH2($connection_array['host'], $connection_array['port']);
        if (file_exists($key)){
            $rsa = new RSA();
            $this->key = file_get_contents($key);
            $rsa->loadKey(file_get_contents($key));
        }
        else if (!empty($key)){
            $rsa = new RSA();
            $this->key = $key;
            $rsa->loadKey($key);
        }
            if ( isset($rsa) and isset($connection_array['pass'])){
                $ssh->login($connection_array['user'], $rsa, $connection_array['pass']);
            }
            elseif ( isset($rsa) ){

                  $ssh->login($connection_array['user'], $rsa );
            }
            else {

                $ssh->login($connection_array['user'], $connection_array['pass'] );
            }

            if (!$ssh->isConnected()) {
                return false;
            }
            $ssh->setTimeOut( 0 );
            $ssh->disableQuietMode();
            $this->ssh = $ssh;
            return $this->ssh->isConnected();
    }
    /**
     @TODO
     Let's automaticly define / create a connection with portforwarding SOURCE=kubernetes://context
     Create the SSH Pod on demand and close it afterwards.
     Create the Keys for the pod on demand.
     **/
     
    //Inject kubeconfig to correct ENV variable
    // Use correct cluster context
    //Forward the SSH port to a local port
    public function configureKube()
    {
        system('mkdir -p $HOME/.kube || true');
        system('echo "$KUBECONFIG_CONTENT" > $HOME/.kube/config');
        system("kubectl config use-context production");
        system("nohup kubectl port-forward svc/ssh 7000:2222 > /dev/null 2>&1 &");
        //sleep 10 seconds so that port forwading can be executed fully
        sleep(10);
    }
}
